# peerplaysjs-fetch-lib

A fetch-based alternative to the WebSocket peerplaysjs-lib

[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2)  

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=mamokin_peerplaysjs-fetch-lib2&metric=alert_status)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2) [![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=mamokin_peerplaysjs-fetch-lib2&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=mamokin_peerplaysjs-fetch-lib2&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=mamokin_peerplaysjs-fetch-lib2&metric=security_rating)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2)  
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=mamokin_peerplaysjs-fetch-lib2&metric=coverage)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2) [![Bugs](https://sonarcloud.io/api/project_badges/measure?project=mamokin_peerplaysjs-fetch-lib2&metric=bugs)](https://sonarcloud.io/dashboard?id=mamokin_peerplaysjs-fetch-lib2)
