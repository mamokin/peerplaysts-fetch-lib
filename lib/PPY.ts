import {PrivateKey} from './index';

const methods = {
  GET_REQUIRED_FEES: 'get_required_fees',
  GET_OBJECTS: 'get_objects',
  GET_FULL_ACCOUNTS: 'get_full_accounts',
  GET_ACCOUNTS: 'get_accounts',
  GET_ASSET: 'lookup_asset_symbols',
  GET_CHAIN_ID: 'get_chain_id',
  BROADCAST: 'broadcast_transaction_with_callback',
};
const ROLES = ['owner', 'active', 'memo'];

const MAINNET_ENDPOINT_1 = 'https://pma.blockveritas.co/ws';
const MAINNET_FAUCET = 'https://faucet.peerplays.download/api/v1/accounts';

const TESTNET_ENDPOINT_1 = '';
const TESTNET_FAUCET = '';

const DEFAULT_PREFIX = 'PPY';
const TESTNET_PREFIX = 'TEST';

// Override these for testnets
const ENDPOINT = TESTNET_ENDPOINT_1;
const FAUCET = TESTNET_FAUCET;
const PREFIX = TESTNET_PREFIX;

if (PREFIX !== DEFAULT_PREFIX) {
  ChainConfig.setPrefix(PREFIX);
}

const ObjectIdRegex = /\d.\d.\d/g;

interface Asset {
  readonly ammount: number;
  readonly asset_id: string;
}

interface AssetObject_core_exchange_rate {
  readonly base: Asset;
  readonly quote: Asset;
}

interface AssetObject_options {
  readonly max_supply: string;
  readonly market_fee_percent: number;
  readonly max_market_fee: string;
  readonly issuer_permissions: number;
  readonly flags: number;
  readonly core_exchange_rate: AssetObject_core_exchange_rate;
  readonly whitelist_authorities: Array<string>;
  readonly blacklist_authorities: Array<string>;
  readonly whitelist_markets: Array<string>;
  readonly blacklist_markets: Array<string>;
  readonly description: Array<string>;
  readonly extensions: Array<string>;
}

interface AssetObject {
  readonly dividend_data_id: string;
  readonly dynamic_asset_data_id: string;
  readonly id: string;
  readonly issuer: string;
  readonly options: AssetObject_options;
  readonly precision: number;
  readonly symbol: string;
}

export default class _PPY {
  /**
   * Convert a human readable asset amount into a blockchain number (no decimals)
   * ie: `1` is `100000` on chain for an asset with precision of 5.
   * @param amount - Amount to convert.
   * @param precision - Precision to convert with.
   */
  static convertToChainAmount(amount: string, precision: number): number|Error {
    if (!amount || !precision) {
      throw new Error('convertToChainAmount: Missing inputs');
    }

    return parseFloat(amount) * Math.pow(10, precision);
  }

  /**
   * Fetch the Peerplays blockchain for data.
   * @param method - The method associated with the request to be made.
   * @param params - The parameters associated with the method.
   * @param api - The data from the request OR an error if there is one.
   */
  static async callChain(method: string, params: string, api = 'database'): Promise<string|Error> {
    const fetchBody = JSON.stringify({
      method: 'call',
      params: [api, method, params],
      jsonrpc: '2.0',
      id: 1,
    });

    return await fetch(ENDPOINT, {
      body: fetchBody,
      method: 'POST',
      headers: {
        Accept: 'application/json',
      },
    })
      .catch(err => {
        throw new Error(err);
      })
      .then(res => res.json())
      .then(res => {
        if (res.result) return res.result;

        if (res.error) {
          throw new Error(res.error.message);
        }

        return res;
      });
  }

  /**
   * Convert the provided WIF to its PrivateKey object counterpart.
   * @param privateKeyWif - Private Key in Wallet Import Format (WIF)
   */
  static privateFromWif(privateKeyWif: string): Buffer {
    return PrivateKey.fromWif(privateKeyWif);
  }

  /**
   * Retrieve the asset information from the Peerplays chain.
   * @param assetID - Optional, defaults to '1.3.0'. The asset id to request information for.
   */
  static async getAsset(assetID: string = '1.3.0'): Promise<AssetObject> {
    if (!assetID) {
      throw new Error('getAsset: Missing inputs');
    }
    const res = await _PPY.callChain(methods.GET_ASSET, [[assetID]]);
    return res[0];
  }

  /**
   * Returns data objects from chain for provided array of object ids.
   * this.getObject(['1.3.0'])
   * @param objIds - The list of ids to retrieve data from the Peerplays chain.
   */
  async getObjects(objIds) {
    if (!objIds || objIds.length === 0) {
      throw new Error('getObjects: Missing inputs');
    }
    return await _PPY.callChain(methods.GET_OBJECTS, [[objIds]]);
  }

  /**
   * Retrieves the chain id.
   */
  static async getChainId() {
    return await this.callChain(methods.GET_CHAIN_ID, []);
  }

  /**
   * Requests from Peerplays blockchain for account data object.
   * @param accountNameOrId - The Peerplays account username to request data for.
   */
  static async getFullAccount(accountNameOrId) {
    if (!accountNameOrId) {
      throw new Error('getFullAccount: Missing input');
    }
    const res = await this.callChain(methods.GET_FULL_ACCOUNTS, [[accountNameOrId], true]);
    return res[0][1].account;
  }

  /**
   * Requests from Peerplays blockchain for full object.
   * @param accountNameOrId - The Peerplays account username to request data for.
   */
  static async getFullAccountObject(accountNameOrId) {
    if (!accountNameOrId) {
      throw new Error('getFullAccount: Missing input');
    }
    const res = await this.callChain(methods.GET_FULL_ACCOUNTS, [[accountNameOrId], true]);
    return res[0][1];
  }

  /**
   * Used by setRequiredFees.
   * @param ops - The operations within a TransactionBuilder instance.
   * @param assetId - The id of the asset to use ie: '1.3.0'.
   * @returns - An array of objects containing the fees associated with the provided `ops`.
   */
  static async getRequiredFees(ops, assetId) {
    if (!ops || !assetId) {
      throw new Error('getRequiredFees: Missing inputs');
    }
    return await _PPY.callChain(methods.GET_REQUIRED_FEES, [ops, assetId]);
  }

  /**
   * Get the requires fees associated with an operation.
   * @param opToPrice
   * @returns fees
   * @memberof _PPY
   */
  async getFees(opToPrice) {
    if (!opToPrice) {
      throw new Error('getFee: Missing inputs');
    }

    const op = ChainTypes.operations[opToPrice];

    if (op === undefined) {
      throw new Error('getFee: No operation matching request');
    }

    // Get the fee schedule
    const obj200 = await this.getObjects('2.0.0');
    const feeSchedule = obj200[0].parameters.current_fees.parameters;

    // Return the fees associated with `opToPrice`
    return feeSchedule[op][1];
  }

  /**
   * By providing a transaction builder instance transaction and the asset to use for the fees, this function will
   * return the fees associated with all operations within said transaction object instance.
   * @param assetId
   * @param tr - The instance of TransactionBuilder associated with the transaction requiring fees to be set.
   */
  static async setRequiredFees(assetId, tr) {
    if (!tr.operations) {
      throw new Error('setRequiredFees: transaction has no operations');
    }

    let operations = [];

    for (let i = 0, len = tr.operations.length; i < len; i++) {
      let op = tr.operations[i];
      operations.push(ops.operation.toObject(op)); // serialize with peerplaysjs-lib
    }

    if (!assetId) {
      let op1_fee = operations[0][1].fee;

      if (op1_fee && op1_fee.asset_id !== null) {
        assetId = op1_fee.asset_id;
      } else {
        assetId = '1.3.0';
      }
    }

    let fees = await this.getRequiredFees(operations, assetId);

    // Proposed transactions need to be flattened
    let flatAssets = [];

    let flatten = obj => {
      if (Array.isArray(obj)) {
        for (let k = 0, len = obj.length; k < len; k++) {
          let item = obj[k];
          flatten(item);
        }
      } else {
        flatAssets.push(obj);
      }
    };

    flatten(fees);

    let assetIndex = 0;

    let setFee = operation => {
      if (
        !operation.fee ||
        operation.fee.amount === 0 ||
        (operation.fee.amount.toString && operation.fee.amount.toString() === '0') // Long
      ) {
        operation.fee = flatAssets[assetIndex];
        // console.log("new operation.fee", operation.fee)
      }
      assetIndex++;
      return operation.fee;
    };

    for (let i = 0; i < operations.length; i++) {
      tr.operations[0][1].fee = setFee(operations[i][1]);
    }

    return tr;
  }

  /**
   * Requests a users' public keys from the Peerplays blockchain.
   * Keys are returned as an array with key order of owner, active, then memo.
   * @param accountNameOrId - ie: 'mcs' || '1.2.26'
   * @returns keys - [ownerPublicKey, activePublicKey, memoPublicKey]
   */
  static async getAccountKeys(accountNameOrId) {
    const keys = {};
    const account = await this.getFullAccount(accountNameOrId);
    ROLES.forEach(role => {
      let key;

      if (role === 'memo') {
        key = [[account.options.memo_key, 1]];
      } else {
        key = account[role].key_auths;
      }

      keys[role] = key;
    });

    return keys;
  }

  /**
   * peerplaysjs-lib.Login will generate keys from provided data and compare them with the ones pulled from the
   * Peerplays blockchain (`userPubKeys`).
   * @param username - The login username to associate with the account to be registered.
   * @param password - The login password to associate with the account to be registered.
   */
  static async authUser(username, password) {
    // Ensure the Login class has the correct roles configured.
    Login.setRoles(ROLES);

    const userPubKeys = await this.getAccountKeys(username);

    const user = {
      accountName: username,
      password,
      auths: userPubKeys,
    };

    // TODO: modify this such that the Scatter UI has the data it requires to import an existing account. Likely will require to generate keys and return them to something
    const authed = Login.checkKeys(user, PREFIX);

    return authed;
  }

  /**
   * Will generate keys from username and password via the peerplaysjs-lib.
   * The public keys for owner, active, and memo are then sent to the faucet that handles account registrations for the configured chain.
   * Once keys are generated and adequate data is provided, a register attempt will be made to the configured faucet endpoint.
   * A Peerplays account password should be generated via randomstring npm package.
   * @param attempt - The number of attempts to start off with.
   * @param username - The login username to associate with the account to be registered.
   * @param password - The login password to associate with the account to be registered.
   * @param referral - Optional referral Peerplays account username.
   * {
   *   account: {
   *     active_key: 'TEST6vw2TA6QXTXWHeoRhq6Sv7F4Pdq5fNkddBGbrY31iCRjEDZnby',
   *     memo_key: 'TEST8C7kCkp6rd3UP4ayVS2o2WyEh9MgrY2Ud4b8SXCWEUfBAspNa6',
   *     name: 'mcs4455',
   *     owner_key: 'TEST7HERrHiogdB5749RahGDKoMHhK3qbwvWABvqpVARrY76b2qcTM',
   *     referrer: 'nathan'
   *  }
   * }
   */
  static async register(attempt, username, password, referral = null) {
    Login.setRoles(ROLES);
    let keys = Login.generateKeys(username, password, ROLES, PREFIX);
    const [ownerPub, activePub, memoPub] = [
      keys.pubKeys.owner,
      keys.pubKeys.active,
      keys.pubKeys.memo,
    ];

    if (!attempt || !username || !password) {
      throw new Error('register: Missing inputs');
    }

    const fetchBody = JSON.stringify({
      account: {
        name: username,
        owner_key: ownerPub,
        active_key: activePub,
        memo_key: memoPub,
        refcode: referral || '',
        referrer: referral,
      },
    });

    // We use a separate fetch here as we want this to potentially have multiple tries.
    return await fetch(FAUCET, {
      method: 'post',
      mode: 'cors',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
      body: fetchBody,
    })
      .then(res => res.json())
      .catch(err => {
        if (attempt > 2) {
          throw new Error(err);
        } else {
          attempt++;
          return this.register(attempt, username, password);
        }
      });
  }

  /**
   * Construct an unsigned transaction for a transfer operation with correct fees.
   * @param from - The sending Peerplays account name.
   * @param to - The recipient Peerplays account name.
   * @param amount - The numerical amount of funds to send to the recipient.
   * @param memo - The optional message to send along with the funds being transferred.
   * @param asset - The Peerplays asset (User Issued Asset token) id associated with the transfer.
   * @param memoWif - Memo key in wallet import format (wif).
   * @param proposeAccount - Optional, default null. The Peerplays account name to be proposed.
   * @param encryptMemo - Optional, default true. Whether or not to encrypt the memo.
   */
  static async getTransferTransaction(
    from,
    to,
    amount,
    memo,
    asset,
    memoWif,
    proposeAccount = null,
    encryptMemo = true,
    optional_nonce = null
  ) {
    let feeAssetId = asset;
    if (!from || !to || !amount || !asset) {
      throw new Error('transfer: Missing inputs');
    }
    let memoToPublicKey;

    // get account data for `from`, `to`, & `proposeAccount`
    const [chainFrom, chainTo] = [await this.getFullAccount(from), await this.getFullAccount(to)];
    const chainProposeAccount = proposeAccount && (await this.getFullAccount(proposeAccount));

    // get asset data
    let chainAsset = await this.getAsset(asset);

    // If we have a non-empty string memo and are configured to encrypt...
    if (memo && encryptMemo) {
      memoToPublicKey = chainTo.options.memo_key;

      // Check for a null memo key, if the memo key is null use the receivers active key
      if (/PPY1111111111111111111111111111111114T1Anm/.test(memoToPublicKey)) {
        memoToPublicKey = chainTo.active.key_auths[0][0];
      }
    }

    let proposeAcountId = proposeAccount ? chainProposeAccount.id : null;
    let memoObject;

    //=================================================================
    // TODO: remove this once we have keys from Scatter to use instead
    //=================================================================
    const wifMemo = memoWif;
    const memoPrivateKey = this.privateFromWif(wifMemo);
    const memoPublicKey = memoPrivateKey.toPublicKey().toPublicKeyString(PREFIX);
    //=================================================================

    if (memo && memoToPublicKey && memoPublicKey) {
      let nonce = optional_nonce == null ? TransactionHelper.unique_nonce_uint64() : optional_nonce;
      const message = Aes.encrypt_with_checksum(
        memoPrivateKey, // From Private Key
        memoToPublicKey, // To Public Key
        nonce,
        memo
      );

      memoObject = {
        from: memoPublicKey, // From Public Key
        to: memoToPublicKey, // To Public Key
        nonce,
        message,
      };
    }

    // Allow user to choose asset with which to pay fees
    let feeAsset = chainAsset;

    // Default to CORE in case of faulty core_exchange_rate
    if (
      feeAsset.options.core_exchange_rate.base.asset_id === '1.3.0' &&
      feeAsset.options.core_exchange_rate.quote.asset_id === '1.3.0'
    ) {
      feeAssetId = '1.3.0';
    }

    let tr = new TransactionBuilder();

    let transferOp = tr.get_type_operation('transfer', {
      fee: {
        amount: 0,
        asset_id: feeAssetId,
      },
      from: chainFrom.id,
      to: chainTo.id,
      amount: {
        amount,
        asset_id: chainAsset.id,
      },
      memo: memoObject,
    });

    if (proposeAccount) {
      let proposalCreateOp = tr.get_type_operation('proposal_create', {
        proposed_ops: [{ op: transferOp }],
        fee_paying_account: proposeAcountId,
      });
      tr.add_operation(proposalCreateOp);
      tr.operations[0][1].expiration_time = parseInt(Date.now() / 1000 + 5);
    } else {
      tr.add_operation(transferOp);
    }

    // Set the transaction fees for the new transaction
    return await this.setRequiredFees(undefined, tr);
  }

  /**
   * Finalize transaction.
   * @param tr - TransactionBuilder instance.
   */
  static async finalize(tr) {
    if (tr.signer_private_keys.length < 1) {
      throw new Error('not signed');
    }

    if (tr.tr_buffer) {
      throw new Error('already finalized');
    }

    const obj210 = await this.callChain(methods.GET_OBJECTS, [['2.1.0']]);
    tr.head_block_time_string = obj210[0].time;

    if (tr.expiration === 0) {
      tr.expiration = tr.base_expiration_sec() + ChainConfig.expire_in_secs;
    }

    tr.ref_block_num = obj210[0].head_block_number & 0xffff;
    tr.ref_block_prefix = Buffer.from(obj210[0].head_block_id, 'hex').readUInt32LE(4);

    let iterable = tr.operations;

    for (let i = 0, len = iterable.length; i < len; i++) {
      let op = iterable[i];

      if (op[1].finalize) {
        op[1] = op[1].finalize();
      }

      // let _type = ops.operation.st_operations[op[0]];
      // let hexBuffer = _type.toBuffer(op[1]).toString('hex');
      // console.log(
      //   'Operation %s: %O => %s (%d bytes)',
      //   _type.operation_name,
      //   op[1],
      //   hexBuffer,
      //   hexBuffer.length / 2
      // );
    }

    tr.tr_buffer = ops.transaction.toBuffer(tr);

    return tr;
  }

  /**
   * Sign the transaction with the keys in `signer_private_keys`
   * @param tr
   * @param chainId
   */
  static async sign(tr, chainId) {
    if (!tr || !chainId) {
      throw new Error('sign: Missing inputs');
    }

    if (!tr.tr_buffer) {
      throw new Error('not finalized');
    }

    if (tr.signatures.length > 0) {
      throw new Error('already signed');
    }

    if (!tr.signer_private_keys.length) {
      throw new Error('Transaction was not signed. Do you have a private key? [no_signers]');
    }

    let end = tr.signer_private_keys.length;

    for (let i = 0; end > 0 ? i < end : i > end; i++) {
      let [private_key, public_key] = tr.signer_private_keys[i];
      let sig = Signature.signBuffer(
        Buffer.concat([Buffer.from(chainId, 'hex'), tr.tr_buffer]),
        private_key,
        public_key
      );
      tr.signatures.push(sig.toBuffer());
    }

    tr.signer_private_keys = [];
    tr.signed = true;
    return tr;
  }

  /**
   * Broadcast the transaction to the chain.
   *
   * @param tr - The transaction to broadcast.
   * @param was_broadcast_callback - The callback to execute once successfully broadcasted.
   */
  static async broadcast(tr, was_broadcast_callback) {
    if (!tr || !was_broadcast_callback) {
      throw new Error('_broadcast: Missing inputs');
    }

    if (tr.signatures.length < 1) {
      const chainId = await this.getChainId();
      tr = await this.sign(tr, chainId);
    }

    if (!tr.tr_buffer) {
      throw new Error('not finalized');
    }

    if (!tr.signatures.length) {
      throw new Error('not signed');
    }

    if (!tr.operations.length) {
      throw new Error('no operations');
    }

    let tr_object = ops.signed_transaction.toObject(tr); // serialize

    return this.callChain(methods.BROADCAST, [res => res, tr_object], 'network_broadcast')
      .then(data => {
        if (was_broadcast_callback) {
          was_broadcast_callback();
        }
      })
      .catch(error => {
        console.log(error);
        let { message } = error;

        if (!message) {
          message = '';
        }

        throw new Error(
          `${message}\n` +
          'peerplays-crypto ' +
          ` digest ${hash
            .sha256(tr.tr_buffer)
            .toString('hex')} transaction ${tr.tr_buffer.toString('hex')} ${JSON.stringify(
              tr_object
            )}`
        );
      });
  }
}

class PPY {
  apiEndpoints: string[];
  initialized: boolean;
  prefix: string;
  constructor(apiEndpoints: string[]) {
    this.apiEndpoints = apiEndpoints;
    this.initialized = false;
    this.prefix = '';
  }

  async init() {
    this.prefix = await this.getPrefix();

    if (this.prefix) {
      this.initialized = true;
    }
  }

  async getPrefix() {
    this.prefix = await _PPY.getAsset();
  }
}