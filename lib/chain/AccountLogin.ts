import PrivateKey from '../ecc/PrivateKey';
import key from '../../ecc/src/KeyUtils';

import {get, set} from './state';

let _keyCachePriv = {};
let _keyCachePub = {};

class AccountLogin {
  get: any;
  set: any;
  subs: {};
  constructor() {
    let state = {loggedIn: false, roles: ['active', 'owner', 'memo']};
    this.get = get(state);
    this.set = set(state);

    this.subs = {};
  }

  addSubscription(cb: string | number) {
    this.subs[cb] = cb;
  }

  setRoles(roles: any) {
    this.set('roles', roles);
  }

  generateKeys(accountName: string, password: string, roles: string[], prefix?: string) {
    if (!accountName || !password) {
      throw new Error('Account name or password required');
    }

    if (password.length < 12) {
      throw new Error('Password must have at least 12 characters');
    }

    let privKeys = {};
    let pubKeys = {};

    (roles || this.get('roles')).forEach((role: string) => {
      let seed = password + accountName + role;
      let pkey = _keyCachePriv[seed]
        ? _keyCachePriv[seed]
        : PrivateKey.fromSeed(key.normalize_brainKey(seed));
      _keyCachePriv[seed] = pkey;

      privKeys[role] = pkey;
      pubKeys[role] = _keyCachePub[seed] ? _keyCachePub[seed] : pkey.toPublicKey().toString(prefix);

      _keyCachePub[seed] = pubKeys[role];
    });

    return {privKeys, pubKeys};
  }

  checkKeys({accountName, password, auths}) {
    if (!accountName || !password || !auths) {
      throw new Error('checkKeys: Missing inputs');
    }

    let hasKey = false;
    let roles = Object.keys(auths);

    for (let i = 0, len = roles.length; i < len; i++) {
      let role = roles[i];
      let {privKeys, pubKeys} = this.generateKeys(accountName, password, [role]);
      auths[role].forEach((roleKey: any[]) => {
        if (roleKey[0] === pubKeys[role]) {
          hasKey = true;
          this.set(role, {priv: privKeys[role], pub: pubKeys[role]});
        }
      });
    }

    if (hasKey) {
      this.set('name', accountName);
    }

    this.set('loggedIn', hasKey);

    return hasKey;
  }

  signTransaction(tr: { add_signer: (arg0: any, arg1: any) => void; }) {
    let hasKey = false;

    this.get('roles').forEach((role: any) => {
      let myKey = this.get(role);

      if (myKey) {
        hasKey = true;
        console.log('adding signer:', myKey.pub);
        tr.add_signer(myKey.priv, myKey.pub);
      }
    });

    if (!hasKey) {
      throw new Error('You do not have any private keys to sign this transaction');
    }
  }
}

export default new AccountLogin();